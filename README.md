# Multiple storages with 2 clients

This setup can be used in one browser and one incognito/private window

## Normal window

- http://localhost:3000
- login as alice@alice.example
- http://localhost:4000
- use localhost:3000 as idp
- logged in to nickel as alice!

## Incognito window

- http://localhost:3000
- login as bob@bob.example
- http://localhost:5000
- use localhost:3000 as idp
- logged in to dime as bob!

## CSS

There is no need to run multiple CSS instances.
A single CSS instance can provide separate storages (pods).

`npm run css`

Starts on http://localhost:3000

### Accounts

- webid: http://localhost:3000/alice/profile/card#me
  - pod: alice
  - directory: css-fs-storage/alice
  - email: alice@alice.example
  - password: password
- webid: http://localhost:3000/bob/profile/card#me
  - pod: bob
  - directory: css-fs-storage/bob
  - email: bob@bob.example
  - password: password

## Penny

We will have two separate instances, each with a distinct
`client_id` and running on a distinct port.
For convenience, both `client_id` will be served from
a dedicated storage served by CSS.

### Nickel

Uses Client ID `http://localhost:3000/nickel/clientid.jsonld`

Start in a separate terminal

`npm run nickel`

http://localhost:4000

### Dime

Uses Client ID `http://localhost:3000/dime/clientid.jsonld`

Start in a separate terminal

`npm run dime`

http://localhost:5000

## Protected Hello

Given the above, as Alice logged in with Nickel

- we can access: [hello-alice](http://localhost:4000/explore/?url=http%3A%2F%2Flocalhost%3A3000%2Fbob%2Fhello-alice) in Bob's storage
- we can access: [hello-alice-nickel](http://localhost:4000/explore/?url=http%3A%2F%2Flocalhost%3A3000%2Fbob%2Fhello-alice-nickel) in Bob's storage
- we can NOT access: [hello-alice-dime](http://localhost:4000/explore/?url=http%3A%2F%2Flocalhost%3A3000%2Fbob%2Fhello-alice-dime) in Bob's storage

Note: After opening each of the links, we may need to click **Connect** in Penny.
